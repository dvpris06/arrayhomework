import Foundation

class Human {
    
    var name: String
    var height: Double
    var weight: Double
    var gender: Gender
    
    enum Gender {
        case male,female
    }
    
    init(name: String, height: Double, weight: Double, gender: Gender) {
        self.name = name
        self.height = height
        self.weight = weight
        self.gender = gender
    }
    
    func move() {
        print("I can move")
    }
}

class Cyclist: Human {
    
    override func move() {
        print("I can ride a bike")
    }
}

class Runner: Human {
    
    override func move() {
        print("I can run")
    }
}

class Swimmer: Human {
    
    override func move() {
        print("I can swim")
    }
}

class Student: Human {
    
    func study() {
        print("I can learn")
    }
    
    func read() {
        print("I can read")
    }
    
    override func move() {
        super.move()
        print("And I can go to university also")
    }
}

let human = Human(name: "Ivan", height: 1.75, weight: 70, gender: .male)
let cyclist = Cyclist(name: "Alexey", height: 1.7, weight: 75, gender: .male)
let runner = Runner(name: "Alice", height: 1.8, weight: 60, gender: .female)
let swimmer = Swimmer(name: "Bob", height: 1.93, weight: 82, gender: .male)
let student = Student(name: "Anna", height: 1.62, weight: 50, gender: .female)

var humanArray = [human, cyclist, runner, swimmer, student]

for human in humanArray.reversed() {
    print("Name: \(human.name)")
    print("Height: \(human.height)")
    print("Weight: \(human.weight)")
    print("Gender: \(human.gender)")
    human.move()
    
    if let student = human as? Student {
        student.study()
        student.read()
    }
    print("")
}

print("---------Master----------")

class Animal {
    
    var name: String
    var color: String
    var isDomestic: Bool
    
    init(name: String, color: String, isDomestic: Bool) {
        self.name = name
        self.color = color
        self.isDomestic = isDomestic
    }
    
    func move() {
        print("I can move like an animal")
    }
    
}

class Dog: Animal {
    
    override func move() {
        print("I can move like a dog")
    }
}

class Bear: Animal {
    
    override func move() {
        print("I can move like a bear")
    }
}

let dog = Dog(name: "Sharik", color: "black", isDomestic: true)
let bear = Bear(name: "Mishka", color: "brown", isDomestic: false)

var array: [Any] = [human, cyclist, runner, swimmer, student, dog, bear]

for element in array {
    print(type(of: element))
    
    if let human = element as? Human {
        print("Name: \(human.name)")
        print("Height: \(human.height)")
        print("Weight: \(human.weight)")
        print("Gender: \(human.gender)")
        human.move()
    } else if let animal = element as? Animal {
        print("Name: \(animal.name)")
        print("Color: \(animal.color)")
        print("Is domestic: \(animal.isDomestic)")
        animal.move()
    }
    print("")
}

print("---------Zvezda----------")

var animalArray = [bear, dog]

for index in 0...max(humanArray.count, animalArray.count){
    
    if index < humanArray.count {
        print("Name: \(humanArray[index].name) \n")
    }
    
    if index < animalArray.count {
        print("Name: \(animalArray[index].name) \n")
    }
}


print("---------Superman----------")

var sortedArray: [Any] = humanArray.sorted { $0.name < $1.name} + animalArray.sorted { $0.name < $1.name}


